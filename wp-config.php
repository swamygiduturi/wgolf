<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wgolf' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'joncjsddntgclena2x5efs578u2uinjphmav6ijxqzdpkm04v4vjg2yrezxl7rkx' );
define( 'SECURE_AUTH_KEY',  'c3e9chcug0kbddgdn7fhgtj2e43jwycdwic15yw6is3mnxa3etr4nslnrwcqqmgj' );
define( 'LOGGED_IN_KEY',    'ypknhbemyupvhdmfrvcz64mejrytdv3etugbtjirelnx16idaztlaz4qep9fatlo' );
define( 'NONCE_KEY',        'yvqklycrsqtbxlnr3ves87xq9xnbft0h93iuaaudn6k9hnalocuc84yoopzvpcmw' );
define( 'AUTH_SALT',        'a9i7gfaurs97t6emawtsrb5dwowtle8gzohtwgiyfprlfjv1qf01glstqg9tslxf' );
define( 'SECURE_AUTH_SALT', '3ez5rzppeqieakgtnz2m7fdoeikxuchdvnoinyufwnyial1sorslzj8ruclbffoi' );
define( 'LOGGED_IN_SALT',   'iuf3oe4qiyycrovlwspqybfxbllszb6nqgtxggnpndgwtmdfuwkx8dzaehgwk05u' );
define( 'NONCE_SALT',       '9ptrr32rsavymtm74otxhiucykogtnxstzxk9qqxipd4pb3wkocz3zd4tk6op2yr' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wpis_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
