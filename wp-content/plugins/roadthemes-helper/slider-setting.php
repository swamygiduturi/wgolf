<?php 

if( ! function_exists( 'road_get_slider_setting' ) ) {
	function road_get_slider_setting() {
		return array(
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Style', 'greenfarm' ),
				'param_name'  => 'style',
				'value'       => array(
					__( 'Grid view', 'greenfarm' )                    => 'product-grid',
					__( 'List view', 'greenfarm' )                    => 'product-list',
					__( 'Countdown', 'greenfarm' )                    => 'product-countdown',
					__( 'Grid view with countdown', 'greenfarm' )     => 'product-grid-countdown',
				),
			),
			array(
				'type'        => 'checkbox',
				'heading'     => __( 'Enable slider', 'greenfarm' ),
				'description' => __( 'If slider is enabled, the "column" ins General group is the number of rows ', 'greenfarm' ),
				'param_name'  => 'enable_slider',
				'value'       => true,
				'save_always' => true, 
				'group'       => __( 'Slider Options', 'greenfarm' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Number of columns (screen: over 1201px)', 'greenfarm' ),
				'param_name' => 'items_1201up',
				'group'      => __( 'Slider Options', 'greenfarm' ),
				'value'      => esc_html__( '4', 'greenfarm' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Number of columns (screen: 993px - 1200px)', 'greenfarm' ),
				'param_name' => 'items_993_1200',
				'group'      => __( 'Slider Options', 'greenfarm' ),
				'value'      => esc_html__( '4', 'greenfarm' ),
			), 
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Number of columns (screen: 769px - 992px)', 'greenfarm' ),
				'param_name' => 'items_769_992',
				'group'      => __( 'Slider Options', 'greenfarm' ),
				'value'      => esc_html__( '3', 'greenfarm' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Number of columns (screen: 641px - 768px)', 'greenfarm' ),
				'param_name' => 'items_641_768',
				'group'      => __( 'Slider Options', 'greenfarm' ),
				'value'      => esc_html__( '2', 'greenfarm' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Number of columns (screen: 481px - 640px)', 'greenfarm' ),
				'param_name' => 'items_481_640',
				'group'      => __( 'Slider Options', 'greenfarm' ),
				'value'      => esc_html__( '2', 'greenfarm' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Number of columns (screen: under 480px)', 'greenfarm' ),
				'param_name' => 'items_0_480',
				'group'      => __( 'Slider Options', 'greenfarm' ),
				'value'      => esc_html__( '1', 'greenfarm' ),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Navigation', 'greenfarm' ),
				'param_name'  => 'navigation',
				'save_always' => true,
				'group'       => __( 'Slider Options', 'greenfarm' ),
				'value'       => array(
					__( 'Yes', 'greenfarm' ) => true,
					__( 'No', 'greenfarm' )  => false,
				),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Pagination', 'greenfarm' ),
				'param_name'  => 'pagination',
				'save_always' => true,
				'group'       => __( 'Slider Options', 'greenfarm' ),
				'value'       => array(
					__( 'No', 'greenfarm' )  => false,
					__( 'Yes', 'greenfarm' ) => true,
				),
			),
			array(
				'type'        => 'textfield',
				'heading'     => __( 'Item Margin (unit:pixel)', 'greenfarm' ),
				'param_name'  => 'item_margin',
				'value'       => 30,
				'save_always' => true,
				'group'       => __( 'Slider Options', 'greenfarm' ),
			),
			array(
				'type'        => 'textfield',
				'heading'     => __( 'Slider speed number (unit: second)', 'greenfarm' ),
				'param_name'  => 'speed',
				'value'       => '500',
				'save_always' => true,
				'group'       => __( 'Slider Options', 'greenfarm' ),
			),
			array(
				'type'        => 'checkbox',
				'heading'     => __( 'Slider loop', 'greenfarm' ),
				'param_name'  => 'loop',
				'value'       => true,
				'group'       => __( 'Slider Options', 'greenfarm' ),
			),
			array(
				'type'        => 'checkbox',
				'heading'     => __( 'Slider Auto', 'greenfarm' ),
				'param_name'  => 'auto',
				'value'       => true,
				'group'       => __( 'Slider Options', 'greenfarm' ),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Navigation style', 'greenfarm' ),
				'param_name'  => 'navigation_style',
				'group'       => __( 'Slider Options', 'greenfarm' ),
				'value'       => array(
					'Navigation top-right'	        => 'navigation-style1',
					'Navigation center horizontal'	=> 'navigation-style2',
				),
			),
		);
	}
}