"use strict";
// product-magnifier var
var greenfarm_magnifier_vars;
var yith_magnifier_options = {
		sliderOptions: {
			responsive: greenfarm_magnifier_vars.responsive,
			circular: greenfarm_magnifier_vars.circular,
			infinite: greenfarm_magnifier_vars.infinite,
			direction: 'up',
			debug: false,
			auto: false,
			align: 'left',
			height: "100%", //turn vertical
			width: 100,
			prev    : {
				button  : "#slider-prev",
				key     : "left"
			},
			next    : {
				button  : "#slider-next",
				key     : "right"
			},
			scroll : {
				items     : 1,
				pauseOnHover: true
			},
			items   : {
				visible: Number(greenfarm_magnifier_vars.visible),
			},
			swipe : {
				onTouch:    true,
				onMouse:    true
			},
			mousewheel : {
				items: 1
			}
		},
		showTitle: false,
		zoomWidth: greenfarm_magnifier_vars.zoomWidth,
		zoomHeight: greenfarm_magnifier_vars.zoomHeight,
		position: greenfarm_magnifier_vars.position,
		lensOpacity: greenfarm_magnifier_vars.lensOpacity,
		softFocus: greenfarm_magnifier_vars.softFocus,
		adjustY: 0,
		disableRightClick: false,
		phoneBehavior: greenfarm_magnifier_vars.phoneBehavior,
		loadingLabel: greenfarm_magnifier_vars.loadingLabel,
	};