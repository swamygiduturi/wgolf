<?php
/**
 * The sidebar for shop page
 *
 * If no active widgets are in the sidebar, hide it completely.
 *
 * @package WordPress
 * @subpackage Greenfarm_Theme
 * @since Greenfarm 1.0
 */
$greenfarm_opt = get_option( 'greenfarm_opt' );
$shopsidebar = 'left';
if(isset($greenfarm_opt['sidebarshop_pos']) && $greenfarm_opt['sidebarshop_pos']!=''){
	$shopsidebar = $greenfarm_opt['sidebarshop_pos'];
}
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$shopsidebar = $_GET['sidebar'];
}
$greenfarm_shop_sidebar_extra_class = NULl;
if($shopsidebar=='left') {
	$greenfarm_shop_sidebar_extra_class = 'order-lg-first';
}
?>
<?php if ( is_active_sidebar( 'sidebar-shop' ) ) : ?>
	<div id="secondary" class="col-12 col-lg-3 sidebar-shop <?php echo esc_attr($greenfarm_shop_sidebar_extra_class);?>">
		<?php dynamic_sidebar( 'sidebar-shop' ); ?>
	</div>
<?php endif; ?>