<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Greenfarm_Theme
 * @since Greenfarm 1.0
 */
$greenfarm_opt = get_option( 'greenfarm_opt' );
get_header();
$greenfarm_bloglayout = 'sidebar';
if(isset($greenfarm_opt['blog_layout']) && $greenfarm_opt['blog_layout']!=''){
	$greenfarm_bloglayout = $greenfarm_opt['blog_layout'];
}
if(isset($_GET['layout']) && $_GET['layout']!=''){
	$greenfarm_bloglayout = $_GET['layout'];
}
$greenfarm_blogsidebar = 'right';
if(isset($greenfarm_opt['sidebarblog_pos']) && $greenfarm_opt['sidebarblog_pos']!=''){
	$greenfarm_blogsidebar = $greenfarm_opt['sidebarblog_pos'];
}
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$greenfarm_blogsidebar = $_GET['sidebar'];
}
if ( !is_active_sidebar( 'sidebar-1' ) )  {
	$greenfarm_bloglayout = 'nosidebar';
}
$greenfarm_blog_main_extra_class = NULl;
if($greenfarm_blogsidebar=='left') {
	$greenfarm_blog_main_extra_class = 'order-lg-last';
}
switch($greenfarm_bloglayout) {
	case 'sidebar':
		$greenfarm_blogclass = 'blog-sidebar';
		$greenfarm_blogcolclass = 9;
		Greenfarm_Class::greenfarm_post_thumbnail_size('greenfarm-post-thumb');
		break;
	case 'largeimage':
		$greenfarm_blogclass = 'blog-large';
		$greenfarm_blogcolclass = 9;
		Greenfarm_Class::greenfarm_post_thumbnail_size('greenfarm-post-thumbwide');
		break;
	case 'grid':
		$greenfarm_blogclass = 'grid';
		$greenfarm_blogcolclass = 9;
		Greenfarm_Class::greenfarm_post_thumbnail_size('greenfarm-post-thumbwide');
		break;
	default:
		$greenfarm_blogclass = 'blog-nosidebar';
		$greenfarm_blogcolclass = 12;
		$greenfarm_blogsidebar = 'none';
		Greenfarm_Class::greenfarm_post_thumbnail_size('greenfarm-post-thumb');
}
?>
<div class="main-container">
	<div class="breadcrumb-container">
		<div class="container">
			<?php Greenfarm_Class::greenfarm_breadcrumb(); ?> 
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-12 <?php echo 'col-lg-'.$greenfarm_blogcolclass; ?> <?php echo esc_attr($greenfarm_blog_main_extra_class);?>">
				<div class="page-content blog-page blogs <?php echo esc_attr($greenfarm_blogclass); if($greenfarm_blogsidebar=='left') {echo ' left-sidebar'; } if($greenfarm_blogsidebar=='right') {echo ' right-sidebar'; } ?>">
					<header class="entry-header">
						<h1 class="entry-title"><?php if(isset($greenfarm_opt) && ($greenfarm_opt !='')) { echo esc_html($greenfarm_opt['blog_header_text']); } else { esc_html_e('Blog', 'greenfarm');}  ?></h1>
					</header>
					<div class="blog-wrapper">
						<?php if ( have_posts() ) : ?>
							<div class="post-container">
								<?php /* Start the Loop */ ?>
								<?php while ( have_posts() ) : the_post(); ?>
									<?php get_template_part( 'content', get_post_format() ); ?>
								<?php endwhile; ?>
							</div>
							<?php Greenfarm_Class::greenfarm_pagination(); ?>
						<?php else : ?>
							<article id="post-0" class="post no-results not-found">
							<?php if ( current_user_can( 'edit_posts' ) ) :
								// Show a different message to a logged-in user who can add posts.
							?>
								<header class="entry-header">
									<h1 class="entry-title"><?php esc_html_e( 'No posts to display', 'greenfarm' ); ?></h1>
								</header>
								<div class="entry-content">
									<p><?php printf( wp_kses(__( 'Ready to publish your first post? <a href="%s">Get started here</a>.', 'greenfarm' ), array('a'=>array('href'=>array()))), admin_url( 'post-new.php' ) ); ?></p>
								</div><!-- .entry-content -->
							<?php else :
								// Show the default message to everyone else.
							?>
								<header class="entry-header">
									<h1 class="entry-title"><?php esc_html_e( 'Nothing Found', 'greenfarm' ); ?></h1>
								</header>
								<div class="entry-content">
									<p><?php esc_html_e( 'Apologies, but no results were found. Perhaps searching will help find a related post.', 'greenfarm' ); ?></p>
									<?php get_search_form(); ?>
								</div><!-- .entry-content -->
							<?php endif; // end current_user_can() check ?>
							</article><!-- #post-0 -->
						<?php endif; // end have_posts() check ?>
					</div>
				</div>
			</div>
			<?php if($greenfarm_bloglayout!='nosidebar') : ?>
				<?php get_sidebar(); ?>
			<?php endif; ?>
		</div>
	</div>
	<!-- brand logo -->
	<?php 
		if(isset($greenfarm_opt['inner_brand']) && function_exists('greenfarm_brands_shortcode') && shortcode_exists( 'ourbrands' ) ){
			if($greenfarm_opt['inner_brand'] && isset($greenfarm_opt['brand_logos'][0]) && $greenfarm_opt['brand_logos'][0]['thumb']!=null) { ?>
				<div class="inner-brands">
					<div class="container">
						<?php if(isset($greenfarm_opt['inner_brand_title']) && $greenfarm_opt['inner_brand_title']!=''){ ?>
							<div class="heading-title style1 ">
								<h3><?php echo esc_html( $greenfarm_opt['inner_brand_title'] ); ?></h3>
							</div>
						<?php } ?>
						<?php echo do_shortcode('[ourbrands]'); ?>
					</div>
				</div>
			<?php }
		}
	?>
	<!-- end brand logo --> 
</div>
<?php get_footer(); ?>