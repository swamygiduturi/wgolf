<?php
/**
 * Template Name: Portfolio Template
 *
 * Description: Portfolio page template
 *
 * @package WordPress
 * @subpackage Greenfarm_Theme
 * @since Greenfarm 1.0
 */
$greenfarm_opt = get_option( 'greenfarm_opt' );
get_header();
?>
<div class="main-container portfolio-page">
	<div class="breadcrumb-container">
		<div class="container">
			<?php Greenfarm_Class::greenfarm_breadcrumb(); ?> 
		</div>
	</div>
	<div class="page-content portfolio-container">
		<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'content', 'page' ); ?>
		<?php endwhile; ?>
	</div>
</div>
<?php get_footer(); ?>