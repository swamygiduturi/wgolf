<?php
if( ! function_exists( 'greenfarm_get_slider_setting' ) ) {
	function greenfarm_get_slider_setting() {
		return array(
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Style', 'greenfarm' ),
				'param_name'  => 'style',
				'value'       => array(
					__( 'Grid view', 'greenfarm' )                    => 'product-grid',
					__( 'List view', 'greenfarm' )                    => 'product-list',
					__( 'Countdown', 'greenfarm' )                    => 'product-countdown',
					__( 'Grid view with countdown', 'greenfarm' )     => 'product-grid-countdown',
				),
			),
			array(
				'type'        => 'checkbox',
				'heading'     => __( 'Enable slider', 'greenfarm' ),
				'description' => __( 'If slider is enabled, the "column" ins General group is the number of rows ', 'greenfarm' ),
				'param_name'  => 'enable_slider',
				'value'       => true,
				'save_always' => true, 
				'group'       => __( 'Slider Options', 'greenfarm' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Number of columns (screen: over 1201px)', 'greenfarm' ),
				'param_name' => 'items_1201up',
				'group'      => __( 'Slider Options', 'greenfarm' ),
				'value'      => esc_html__( '4', 'greenfarm' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Number of columns (screen: 993px - 1200px)', 'greenfarm' ),
				'param_name' => 'items_993_1200',
				'group'      => __( 'Slider Options', 'greenfarm' ),
				'value'      => esc_html__( '4', 'greenfarm' ),
			), 
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Number of columns (screen: 769px - 992px)', 'greenfarm' ),
				'param_name' => 'items_769_992',
				'group'      => __( 'Slider Options', 'greenfarm' ),
				'value'      => esc_html__( '3', 'greenfarm' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Number of columns (screen: 641px - 768px)', 'greenfarm' ),
				'param_name' => 'items_641_768',
				'group'      => __( 'Slider Options', 'greenfarm' ),
				'value'      => esc_html__( '2', 'greenfarm' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Number of columns (screen: 481px - 640px)', 'greenfarm' ),
				'param_name' => 'items_481_640',
				'group'      => __( 'Slider Options', 'greenfarm' ),
				'value'      => esc_html__( '2', 'greenfarm' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Number of columns (screen: under 480px)', 'greenfarm' ),
				'param_name' => 'items_0_480',
				'group'      => __( 'Slider Options', 'greenfarm' ),
				'value'      => esc_html__( '1', 'greenfarm' ),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Navigation', 'greenfarm' ),
				'param_name'  => 'navigation',
				'save_always' => true,
				'group'       => __( 'Slider Options', 'greenfarm' ),
				'value'       => array(
					__( 'Yes', 'greenfarm' ) => true,
					__( 'No', 'greenfarm' )  => false,
				),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Pagination', 'greenfarm' ),
				'param_name'  => 'pagination',
				'save_always' => true,
				'group'       => __( 'Slider Options', 'greenfarm' ),
				'value'       => array(
					__( 'No', 'greenfarm' )  => false,
					__( 'Yes', 'greenfarm' ) => true,
				),
			),
			array(
				'type'        => 'textfield',
				'heading'     => __( 'Item Margin (unit:pixel)', 'greenfarm' ),
				'param_name'  => 'item_margin',
				'value'       => 30,
				'save_always' => true,
				'group'       => __( 'Slider Options', 'greenfarm' ),
			),
			array(
				'type'        => 'textfield',
				'heading'     => __( 'Slider speed number (unit: second)', 'greenfarm' ),
				'param_name'  => 'speed',
				'value'       => '500',
				'save_always' => true,
				'group'       => __( 'Slider Options', 'greenfarm' ),
			),
			array(
				'type'        => 'checkbox',
				'heading'     => __( 'Slider loop', 'greenfarm' ),
				'param_name'  => 'loop',
				'value'       => true,
				'group'       => __( 'Slider Options', 'greenfarm' ),
			),
			array(
				'type'        => 'checkbox',
				'heading'     => __( 'Slider Auto', 'greenfarm' ),
				'param_name'  => 'auto',
				'value'       => true,
				'group'       => __( 'Slider Options', 'greenfarm' ),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Navigation style', 'greenfarm' ),
				'param_name'  => 'navigation_style',
				'group'       => __( 'Slider Options', 'greenfarm' ),
				'value'       => array(
					'Navigation top-right'	        => 'navigation-style1',
					'Navigation center horizontal'	=> 'navigation-style2',
				),
			),
		);
	}
}
//Shortcodes for Visual Composer
add_action( 'vc_before_init', 'greenfarm_vc_shortcodes' );
function greenfarm_vc_shortcodes() { 
	//Main Menu
	vc_map( array(
		'name'        => esc_html__( 'Main Menu', 'greenfarm'),
		'description' => __( 'Set Primary Menu in Apperance - Menus - Manage Locations', 'greenfarm' ),
		'base'        => 'roadmainmenu',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'greenfarm'),
		"icon"        => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(
			array(
				'type'       => 'attach_image',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Upload sticky logo image', 'greenfarm' ),
				'param_name' => 'sticky_logoimage',
				'value'      => '',
			),
		)
	) );
	//Categories Menu
	vc_map( array(
		'name'        => esc_html__( 'Categories Menu', 'greenfarm'),
		'description' => __( 'Set Categories Menu in Apperance - Menus - Manage Locations', 'greenfarm' ),
		'base'        => 'roadcategoriesmenu',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'greenfarm'),
		"icon"        => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(),
	) );
	//Social Icons
	vc_map( array(
		'name'        => esc_html__( 'Social Icons', 'greenfarm'),
		'description' => __( 'Configure icons and links in Theme Options', 'greenfarm' ),
		'base'        => 'roadsocialicons',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'greenfarm'),
		"icon"        => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(),
	) );
	//Mini Cart
	vc_map( array(
		'name'        => esc_html__( 'Mini Cart', 'greenfarm'),
		'description' => __( 'Mini Cart', 'greenfarm' ),
		'base'        => 'roadminicart',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'greenfarm'),
		"icon"        => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(),
	) );
	//Products Search without dropdown
	vc_map( array(
		'name'        => esc_html__( 'Product Search (No dropdown)', 'greenfarm'),
		'description' => __( 'Product Search (No dropdown)', 'greenfarm' ),
		'base'        => 'roadproductssearch',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'greenfarm'),
		"icon"        => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(),
	) );
	//Products Search with dropdown
	vc_map( array(
		'name'        => esc_html__( 'Product Search (Dropdown)', 'greenfarm'),
		'description' => __( 'Product Search (Dropdown)', 'greenfarm' ),
		'base'        => 'roadproductssearchdropdown',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'greenfarm'),
		"icon"        => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(),
	) );
	//Image slider
	vc_map( array(
		'name'        => esc_html__( 'Image slider', 'greenfarm' ),
		'description' => __( 'Upload images and links in Theme Options', 'greenfarm' ),
		'base'        => 'image_slider',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'greenfarm'),
		"icon"        => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number of rows', 'greenfarm' ),
				'param_name' => 'rows',
				'value'      => array(
					'1'	=> '1',
					'2'	=> '2',
					'3'	=> '3',
					'4'	=> '4',
				),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => __( 'Number of columns (screen: over 1201px)', 'greenfarm' ),
				'param_name' => 'items_1201up',
				'value'      => esc_html__( '4', 'greenfarm' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => __( 'Number of columns (screen: 993px - 1200px)', 'greenfarm' ),
				'param_name' => 'items_993_1200',
				'value'      => esc_html__( '4', 'greenfarm' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => __( 'Number of columns (screen: 769px - 992px)', 'greenfarm' ),
				'param_name' => 'items_769_992',
				'value'      => esc_html__( '3', 'greenfarm' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => __( 'Number of columns (screen: 641px - 768px)', 'greenfarm' ),
				'param_name' => 'items_641_768',
				'value'      => esc_html__( '2', 'greenfarm' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => __( 'Number of columns (screen: 481px - 640px)', 'greenfarm' ),
				'param_name' => 'items_481_640',
				'value'      => esc_html__( '2', 'greenfarm' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => __( 'Number of columns (screen: under 480px)', 'greenfarm' ),
				'param_name' => 'items_0_480',
				'value'      => esc_html__( '1', 'greenfarm' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => __( 'Navigation', 'greenfarm' ),
				'param_name' => 'navigation',
				'value'      => array(
					__( 'Yes', 'greenfarm' ) => true,
					__( 'No', 'greenfarm' )  => false,
				),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => __( 'Pagination', 'greenfarm' ),
				'param_name' => 'pagination',
				'value'      => array(
					__( 'No', 'greenfarm' )  => false,
					__( 'Yes', 'greenfarm' ) => true,
				),
			),
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Item Margin (unit:pixel)', 'greenfarm' ),
				'param_name' => 'item_margin',
				'value'      => 30,
			),
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Slider speed number (unit: second)', 'greenfarm' ),
				'param_name' => 'speed',
				'value'      => '500',
			),
			array(
				'type'       => 'checkbox',
				'value'      => true,
				'heading'    => __( 'Slider loop', 'greenfarm' ),
				'param_name' => 'loop',
			),
			array(
				'type'       => 'checkbox',
				'value'      => true,
				'heading'    => __( 'Slider Auto', 'greenfarm' ),
				'param_name' => 'auto',
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Style', 'greenfarm' ),
				'param_name' => 'style',
				'value'      => array(
					__( 'Style 1', 'greenfarm' )  => 'style1',
				),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Navigation style', 'greenfarm' ),
				'param_name'  => 'navigation_style',
				'value'       => array(
					__( 'Navigation top-right', 'greenfarm' )          => 'navigation-style1',
					__( 'Navigation center horizontal', 'greenfarm' )  => 'navigation-style2',
				),
			),
		),
	) );
	//Brand logos
	vc_map( array(
		'name'        => esc_html__( 'Brand Logos', 'greenfarm' ),
		'description' => __( 'Upload images and links in Theme Options', 'greenfarm' ),
		'base'        => 'ourbrands',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'greenfarm'),
		"icon"        => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number of rows', 'greenfarm' ),
				'param_name' => 'rows',
				'value'      => array(
					'1'	=> '1',
					'2'	=> '2',
					'3'	=> '3',
					'4'	=> '4',
				),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => __( 'Number of columns (screen: over 1201px)', 'greenfarm' ),
				'param_name' => 'items_1201up',
				'value'      => esc_html__( '5', 'greenfarm' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => __( 'Number of columns (screen: 993px - 1200px)', 'greenfarm' ),
				'param_name' => 'items_993_1200',
				'value'      => esc_html__( '5', 'greenfarm' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => __( 'Number of columns (screen: 769px - 992px)', 'greenfarm' ),
				'param_name' => 'items_769_992',
				'value'      => esc_html__( '4', 'greenfarm' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => __( 'Number of columns (screen: 641px - 768px)', 'greenfarm' ),
				'param_name' => 'items_641_768',
				'value'      => esc_html__( '3', 'greenfarm' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => __( 'Number of columns (screen: 481px - 640px)', 'greenfarm' ),
				'param_name' => 'items_481_640',
				'value'      => esc_html__( '2', 'greenfarm' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => __( 'Number of columns (screen: under 480px)', 'greenfarm' ),
				'param_name' => 'items_0_480',
				'value'      => esc_html__( '1', 'greenfarm' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => __( 'Navigation', 'greenfarm' ),
				'param_name' => 'navigation',
				'value'      => array(
					__( 'Yes', 'greenfarm' ) => true,
					__( 'No', 'greenfarm' )  => false,
				),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => __( 'Pagination', 'greenfarm' ),
				'param_name' => 'pagination',
				'value'      => array(
					__( 'No', 'greenfarm' )  => false,
					__( 'Yes', 'greenfarm' ) => true,
				),
			),
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Item Margin (unit:pixel)', 'greenfarm' ),
				'param_name' => 'item_margin',
				'value'      => 0,
			),
			array(
				'type'       => 'textfield',
				'heading'    =>  __( 'Slider speed number (unit: second)', 'greenfarm' ),
				'param_name' => 'speed',
				'value'      => '500',
			),
			array(
				'type'       => 'checkbox',
				'value'      => true,
				'heading'    => __( 'Slider loop', 'greenfarm' ),
				'param_name' => 'loop',
			),
			array(
				'type'       => 'checkbox',
				'value'      => true,
				'heading'    => __( 'Slider Auto', 'greenfarm' ),
				'param_name' => 'auto',
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Style', 'greenfarm' ),
				'param_name' => 'style',
				'value'      => array(
					__( 'Style 1', 'greenfarm' )       => 'style1',
				),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Navigation style', 'greenfarm' ),
				'param_name'  => 'navigation_style',
				'value'       => array(
					__( 'Navigation top-right', 'greenfarm' )          => 'navigation-style1',
					__( 'Navigation center horizontal', 'greenfarm' )  => 'navigation-style2',
				),
			),
		),
	) );
	//Latest posts
	vc_map( array(
		'name'        => esc_html__( 'Latest posts', 'greenfarm' ),
		'description' => __( 'List posts', 'greenfarm' ),
		'base'        => 'latestposts',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'greenfarm'),
		"icon"        => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number of posts', 'greenfarm' ),
				'param_name' => 'posts_per_page',
				'value'      => esc_html__( '10', 'greenfarm' ),
			),
			array(
				'type'        => 'textfield',
				'holder'      => 'div',
				'class'       => '',
				'heading'     => esc_html__( 'Category', 'greenfarm' ),
				'param_name'  => 'category',
				'value'       => esc_html__( '0', 'greenfarm' ),
				'description' => esc_html__( 'ID/slug of the category. Default is 0 : show all posts', 'greenfarm' ),
			),
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Image scale', 'greenfarm' ),
				'param_name' => 'image',
				'value'      => array(
					'Wide'	=> 'wide',
					'Square'=> 'square',
				),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Excerpt length', 'greenfarm' ),
				'param_name' => 'length',
				'value'      => esc_html__( '20', 'greenfarm' ),
			),
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number of columns', 'greenfarm' ),
				'param_name' => 'colsnumber',
				'value'      => array(
					'1'	=> '1',
					'2'	=> '2',
					'3'	=> '3',
					'4'	=> '4',
				),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Style', 'greenfarm' ),
				'param_name'  => 'style',
				'value'       => array(
					__( 'Style 1', 'greenfarm' )  => 'style1',
				),
			),
			array(
				'type'        => 'checkbox',
				'heading'     => __( 'Enable slider', 'greenfarm' ),
				'param_name'  => 'enable_slider',
				'value'       => true,
				'save_always' => true, 
				'group'       => __( 'Slider Options', 'greenfarm' ),
			),
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number of rows', 'greenfarm' ),
				'param_name' => 'rowsnumber',
				'group'      => __( 'Slider Options', 'greenfarm' ),
				'value'      => array(
						'1'	=> '1',
						'2'	=> '2',
						'3'	=> '3',
						'4'	=> '4',
					),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => __( 'Number of columns (screen: 993px - 1200px)', 'greenfarm' ),
				'param_name' => 'items_993_1200',
				'value'      => esc_html__( '3', 'greenfarm' ),
				'group'       => __( 'Slider Options', 'greenfarm' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => __( 'Number of columns (screen: 769px - 992px)', 'greenfarm' ),
				'param_name' => 'items_769_992',
				'value'      => esc_html__( '3', 'greenfarm' ),
				'group'       => __( 'Slider Options', 'greenfarm' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => __( 'Number of columns (screen: 641px - 768px)', 'greenfarm' ),
				'param_name' => 'items_641_768',
				'value'      => esc_html__( '2', 'greenfarm' ),
				'group'       => __( 'Slider Options', 'greenfarm' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => __( 'Number of columns (screen: 481px - 640px)', 'greenfarm' ),
				'param_name' => 'items_481_640',
				'value'      => esc_html__( '2', 'greenfarm' ),
				'group'       => __( 'Slider Options', 'greenfarm' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => __( 'Number of columns (screen: under 480px)', 'greenfarm' ),
				'param_name' => 'items_0_480',
				'value'      => esc_html__( '1', 'greenfarm' ),
				'group'       => __( 'Slider Options', 'greenfarm' ),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Navigation', 'greenfarm' ),
				'param_name'  => 'navigation',
				'save_always' => true,
				'group'       => __( 'Slider Options', 'greenfarm' ),
				'value'       => array(
					__( 'Yes', 'greenfarm' ) => true,
					__( 'No', 'greenfarm' )  => false,
				),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Pagination', 'greenfarm' ),
				'param_name'  => 'pagination',
				'save_always' => true,
				'group'       => __( 'Slider Options', 'greenfarm' ),
				'value'       => array(
					__( 'No', 'greenfarm' )  => false,
					__( 'Yes', 'greenfarm' ) => true,
				),
			),
			array(
				'type'        => 'textfield',
				'heading'     => __( 'Item Margin (unit:pixel)', 'greenfarm' ),
				'param_name'  => 'item_margin',
				'value'       => 30,
				'save_always' => true,
				'group'       => __( 'Slider Options', 'greenfarm' ),
			),
			array(
				'type'        => 'textfield',
				'heading'     => __( 'Slider speed number (unit: second)', 'greenfarm' ),
				'param_name'  => 'speed',
				'value'       => '500',
				'save_always' => true,
				'group'       => __( 'Slider Options', 'greenfarm' ),
			),
			array(
				'type'        => 'checkbox',
				'heading'     => __( 'Slider loop', 'greenfarm' ),
				'param_name'  => 'loop',
				'value'       => true,
				'group'       => __( 'Slider Options', 'greenfarm' ),
			),
			array(
				'type'        => 'checkbox',
				'heading'     => __( 'Slider Auto', 'greenfarm' ),
				'param_name'  => 'auto',
				'value'       => true,
				'group'       => __( 'Slider Options', 'greenfarm' ),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Navigation style', 'greenfarm' ),
				'param_name'  => 'navigation_style',
				'group'       => __( 'Slider Options', 'greenfarm' ),
				'value'       => array(
					__( 'Navigation center horizontal', 'greenfarm' )  => 'navigation-style1',
					__( 'Navigation top-right', 'greenfarm' )          => 'navigation-style2',
				),
			),
		),
	) );
	//Testimonials
	vc_map( array(
		'name'        => esc_html__( 'Testimonials', 'greenfarm' ),
		'description' => __( 'Testimonial slider', 'greenfarm' ),
		'base'        => 'woothemes_testimonials',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'greenfarm'),
		"icon"     	  => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number of testimonial', 'greenfarm' ),
				'param_name' => 'limit',
				'value'      => esc_html__( '10', 'greenfarm' ),
			),
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Display Author', 'greenfarm' ),
				'param_name' => 'display_author',
				'value'      => array(
					'Yes'	=> 'true',
					'No'	=> 'false',
				),
			),
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Display Avatar', 'greenfarm' ),
				'param_name' => 'display_avatar',
				'value'      => array(
					'Yes'=> 'true',
					'No' => 'false',
				),
			),
			array(
				'type'        => 'textfield',
				'holder'      => 'div',
				'class'       => '',
				'heading'     => esc_html__( 'Avatar image size', 'greenfarm' ),
				'param_name'  => 'size',
				'value'       => '150',
				'description' => esc_html__( 'Avatar image size in pixels. Default is 150', 'greenfarm' ),
			),
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Display URL', 'greenfarm' ),
				'param_name' => 'display_url',
				'value'      => array(
					'Yes'	=> 'true',
					'No'	=> 'false',
				),
			),
			array(
				'type'        => 'textfield',
				'holder'      => 'div',
				'class'       => '',
				'heading'     => esc_html__( 'Category', 'greenfarm' ),
				'param_name'  => 'category',
				'value'       => esc_html__( '0', 'greenfarm' ),
				'description' => esc_html__( 'ID/slug of the category. Default is 0 : show all testimonials', 'greenfarm' ),
			),
		),
	) );
	//Counter
	vc_map( array(
		'name'     => esc_html__( 'Counter', 'greenfarm' ),
		'description' => __( 'Counter', 'greenfarm' ),
		'base'     => 'greenfarm_counter',
		'class'    => '',
		'category' => esc_html__( 'Theme', 'greenfarm'),
		"icon"     => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'   => array(
			array(
				'type'        => 'attach_image',
				'holder'      => 'div',
				'class'       => '',
				'heading'     => esc_html__( 'Image icon', 'greenfarm' ),
				'param_name'  => 'image',
				'value'       => '',
				'description' => esc_html__( 'Upload icon image', 'greenfarm' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number', 'greenfarm' ),
				'param_name' => 'number',
				'value'      => '',
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Text', 'greenfarm' ),
				'param_name' => 'text',
				'value'      => '',
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Style', 'greenfarm' ),
				'param_name'  => 'style',
				'value'       => array(
					__( 'Style 1', 'greenfarm' )   => 'style1',
				),
			),
		),
	) );
	//Heading title
	vc_map( array(
		'name'     => esc_html__( 'Heading Title', 'greenfarm' ),
		'description' => __( 'Heading Title', 'greenfarm' ),
		'base'     => 'roadthemes_title',
		'class'    => '',
		'category' => esc_html__( 'Theme', 'greenfarm'),
		"icon"     => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'   => array(
			array(
				'type'       => 'textarea',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Heading title element', 'greenfarm' ),
				'param_name' => 'heading_title',
				'value'      => 'Title',
			),
			array(
				'type'       => 'textarea',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Heading sub-title element', 'greenfarm' ),
				'param_name' => 'sub_heading_title',
				'value'      => '',
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Style', 'greenfarm' ),
				'param_name'  => 'style',
				'value'       => array(
					__( 'Style 1 (Default)', 'greenfarm' )       => 'style1',
					__( 'Style 2 (Pink color)', 'greenfarm' )    => 'style2',
					__( 'Style 3 (Orange color)', 'greenfarm' )  => 'style3',
					__( 'Style 4 (Brown color)', 'greenfarm' )   => 'style4',
					__( 'Style 5', 'greenfarm' )    	         => 'style5',
					__( 'Style 6 (Footer)', 'greenfarm' )        => 'style6',
					__( 'Style 7 (Footer)', 'greenfarm' )        => 'style7',
				),
			),
		),
	) );
	//Countdown
	vc_map( array(
		'name'     => esc_html__( 'Countdown', 'greenfarm' ),
		'description' => __( 'Countdown', 'greenfarm' ),
		'base'     => 'roadthemes_countdown',
		'class'    => '',
		'category' => esc_html__( 'Theme', 'greenfarm'),
		"icon"     => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'   => array(
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'End date (day)', 'greenfarm' ),
				'param_name' => 'countdown_day',
				'value'      => '1',
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'End date (month)', 'greenfarm' ),
				'param_name' => 'countdown_month',
				'value'      => '1',
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'End date (year)', 'greenfarm' ),
				'param_name' => 'countdown_year',
				'value'      => '2020',
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Style', 'greenfarm' ),
				'param_name'  => 'style',
				'value'       => array(
					__( 'Style 1', 'greenfarm' )      => 'style1',
				),
			),
		),
	) );
}
?>