<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Greenfarm_Theme
 * @since Greenfarm 1.0
 */
$greenfarm_opt = get_option( 'greenfarm_opt' );
get_header();
$greenfarm_bloglayout = 'sidebar';
if(isset($greenfarm_opt['blog_layout']) && $greenfarm_opt['blog_layout']!=''){
	$greenfarm_bloglayout = $greenfarm_opt['blog_layout'];
}
if(isset($_GET['layout']) && $_GET['layout']!=''){
	$greenfarm_bloglayout = $_GET['layout'];
}
$greenfarm_blogsidebar = 'right';
if(isset($greenfarm_opt['sidebarblog_pos']) && $greenfarm_opt['sidebarblog_pos']!=''){
	$greenfarm_blogsidebar = $greenfarm_opt['sidebarblog_pos'];
}
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$greenfarm_blogsidebar = $_GET['sidebar'];
}
if ( !is_active_sidebar( 'sidebar-1' ) )  {
	$greenfarm_bloglayout = 'nosidebar';
}
switch($greenfarm_bloglayout) {
	case 'sidebar':
		$greenfarm_blogclass = 'blog-sidebar';
		$greenfarm_blogcolclass = 9;
		break;
	default:
		$greenfarm_blogclass = 'blog-nosidebar'; //for both fullwidth and no sidebar
		$greenfarm_blogcolclass = 12;
		$greenfarm_blogsidebar = 'none';
}
?>
<div class="main-container">
	<div class="breadcrumb-container">
		<div class="container">
			<?php Greenfarm_Class::greenfarm_breadcrumb(); ?> 
		</div>
	</div>
	<div class="container">
		<div class="row">
			<?php
			$customsidebar = get_post_meta( $post->ID, '_greenfarm_custom_sidebar', true );
			$customsidebar_pos = get_post_meta( $post->ID, '_greenfarm_custom_sidebar_pos', true );
			if($customsidebar != ''){
				if($customsidebar_pos == 'left' && is_active_sidebar( $customsidebar ) ) {
					echo '<div id="secondary" class="col-12 col-lg-3">';
						dynamic_sidebar( $customsidebar );
					echo '</div>';
				} 
			} else {
				if($greenfarm_blogsidebar=='left') {
					get_sidebar();
				}
			} ?>
			<div class="col-12 <?php echo 'col-lg-'.$greenfarm_blogcolclass; ?>">
				<div class="page-content blog-page single <?php echo esc_attr($greenfarm_blogclass); if($greenfarm_blogsidebar=='left') {echo ' left-sidebar'; } if($greenfarm_blogsidebar=='right') {echo ' right-sidebar'; } ?>">
					<?php while ( have_posts() ) : the_post(); ?>
						<?php get_template_part( 'content', get_post_format() ); ?>
						<?php comments_template( '', true ); ?>
					<?php endwhile; // end of the loop. ?>
				</div>
			</div>
			<?php
			if($customsidebar != ''){
				if($customsidebar_pos == 'right' && is_active_sidebar( $customsidebar ) ) {
					echo '<div id="secondary" class="col-12 col-lg-3">';
						dynamic_sidebar( $customsidebar );
					echo '</div>';
				} 
			} else {
				if($greenfarm_blogsidebar=='right') {
					get_sidebar();
				}
			} ?>
		</div>
	</div> 
	<!-- brand logo -->
	<?php 
		if(isset($greenfarm_opt['inner_brand']) && function_exists('greenfarm_brands_shortcode') && shortcode_exists( 'ourbrands' ) ){
			if($greenfarm_opt['inner_brand'] && isset($greenfarm_opt['brand_logos'][0]) && $greenfarm_opt['brand_logos'][0]['thumb']!=null) { ?>
				<div class="inner-brands">
					<div class="container">
						<?php if(isset($greenfarm_opt['inner_brand_title']) && $greenfarm_opt['inner_brand_title']!=''){ ?>
							<div class="heading-title style1 ">
								<h3><?php echo esc_html( $greenfarm_opt['inner_brand_title'] ); ?></h3>
							</div>
						<?php } ?>
						<?php echo do_shortcode('[ourbrands]'); ?>
					</div>
				</div>
			<?php }
		}
	?>
	<!-- end brand logo --> 
</div>
<?php get_footer(); ?>