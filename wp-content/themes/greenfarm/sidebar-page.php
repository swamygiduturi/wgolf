<?php
/**
 * The sidebar for content page
 *
 * If no active widgets are in the sidebar, hide it completely.
 *
 * @package WordPress
 * @subpackage Greenfarm_Theme
 * @since Greenfarm 1.0
 */
$greenfarm_opt = get_option( 'greenfarm_opt' );
$greenfarm_page_sidebar_extra_class = NULl;
if($greenfarm_opt['sidebarse_pos']=='left') {
	$greenfarm_page_sidebar_extra_class = 'order-lg-first';
}
?>
<?php if ( is_active_sidebar( 'sidebar-page' ) ) : ?>
<div id="secondary" class="col-12 col-lg-3 <?php echo esc_attr($greenfarm_page_sidebar_extra_class);?>">
	<?php dynamic_sidebar( 'sidebar-page' ); ?>
</div>
<?php endif; ?>