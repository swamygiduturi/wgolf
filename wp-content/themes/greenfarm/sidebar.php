<?php
/**
 * The sidebar containing the main widget area
 *
 * If no active widgets are in the sidebar, hide it completely.
 *
 * @package WordPress
 * @subpackage Greenfarm_Theme
 * @since Greenfarm 1.0
 */
$greenfarm_opt = get_option( 'greenfarm_opt' );
$greenfarm_blogsidebar = 'right';
if(isset($greenfarm_opt['sidebarblog_pos']) && $greenfarm_opt['sidebarblog_pos']!=''){
	$greenfarm_blogsidebar = $greenfarm_opt['sidebarblog_pos'];
}
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$greenfarm_blogsidebar = $_GET['sidebar'];
}
$greenfarm_blog_sidebar_extra_class = NULl;
if($greenfarm_blogsidebar=='left') {
	$greenfarm_blog_sidebar_extra_class = 'order-lg-first';
}
?>
<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
	<div id="secondary" class="col-12 col-lg-3 <?php echo esc_attr($greenfarm_blog_sidebar_extra_class);?>">
		<?php dynamic_sidebar( 'sidebar-1' ); ?>
		</div>
	</div><!-- #secondary -->
<?php endif; ?>