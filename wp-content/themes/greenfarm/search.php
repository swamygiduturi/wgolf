<?php
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage Greenfarm_Theme
 * @since Greenfarm 1.0
 */
$greenfarm_opt = get_option( 'greenfarm_opt' );
get_header();
$greenfarm_bloglayout = 'sidebar';
if(isset($greenfarm_opt['blog_layout']) && $greenfarm_opt['blog_layout']!=''){
	$greenfarm_bloglayout = $greenfarm_opt['blog_layout'];
}
if(isset($_GET['layout']) && $_GET['layout']!=''){
	$greenfarm_bloglayout = $_GET['layout'];
}
$greenfarm_blogsidebar = 'right';
if(isset($greenfarm_opt['sidebarblog_pos']) && $greenfarm_opt['sidebarblog_pos']!=''){
	$greenfarm_blogsidebar = $greenfarm_opt['sidebarblog_pos'];
}
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$greenfarm_blogsidebar = $_GET['sidebar'];
}
if ( !is_active_sidebar( 'sidebar-1' ) )  {
	$greenfarm_bloglayout = 'nosidebar';
}
$greenfarm_mainextraclass = NULl;
if($greenfarm_blogsidebar=='left') {
	$greenfarm_mainextraclass = 'order-lg-last';
}
switch($greenfarm_bloglayout) {
	case 'sidebar':
		$greenfarm_blogclass = 'blog-sidebar';
		$greenfarm_blogcolclass = 9;
		Greenfarm_Class::greenfarm_post_thumbnail_size('greenfarm-post-thumb');
		break;
	case 'largeimage':
		$greenfarm_blogclass = 'blog-large';
		$greenfarm_blogcolclass = 9;
		Greenfarm_Class::greenfarm_post_thumbnail_size('greenfarm-post-thumbwide');
		break;
	case 'grid':
		$greenfarm_blogclass = 'grid';
		$greenfarm_blogcolclass = 9;
		Greenfarm_Class::greenfarm_post_thumbnail_size('greenfarm-post-thumbwide');
		break;
	default:
		$greenfarm_blogclass = 'blog-nosidebar';
		$greenfarm_blogcolclass = 12;
		$greenfarm_blogsidebar = 'none';
		Greenfarm_Class::greenfarm_post_thumbnail_size('greenfarm-post-thumb');
}
?>
<div class="main-container">
	<div class="breadcrumb-container">
		<div class="container">
			<?php Greenfarm_Class::greenfarm_breadcrumb(); ?>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-12 <?php echo 'col-lg-'.$greenfarm_blogcolclass; ?> <?php echo esc_attr($greenfarm_mainextraclass);?>">
				<div class="page-content blog-page blogs <?php echo esc_attr($greenfarm_blogclass); if($greenfarm_blogsidebar=='left') {echo ' left-sidebar'; } if($greenfarm_blogsidebar=='right') {echo ' right-sidebar'; } ?>">
					<header class="entry-header">
						<h2 class="entry-title"><?php printf( wp_kses(__( 'Search Results for: %s', 'greenfarm' ), array('span'=>array())), '<span>' . get_search_query() . '</span>' ); ?></h2>
					</header>
					<?php if ( have_posts() ) : ?>
						<div class="post-container">
							<?php /* Start the Loop */ ?>
							<?php while ( have_posts() ) : the_post(); ?>
								<?php get_template_part( 'content', get_post_format() ); ?>
							<?php endwhile; ?>
						</div>
						<?php Greenfarm_Class::greenfarm_pagination(); ?>
					<?php else : ?>
						<article id="post-0" class="post no-results not-found">
							<header class="entry-header">
								<h1 class="entry-title"><?php esc_html_e( 'Nothing Found', 'greenfarm' ); ?></h1>
							</header>
							<div class="entry-content">
								<p><?php esc_html_e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'greenfarm' ); ?></p>
								<?php get_search_form(); ?>
							</div><!-- .entry-content -->
						</article><!-- #post-0 -->
					<?php endif; ?>
				</div>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
	<!-- brand logo -->
	<?php 
		if(isset($greenfarm_opt['inner_brand']) && function_exists('greenfarm_brands_shortcode') && shortcode_exists( 'ourbrands' ) ){
			if($greenfarm_opt['inner_brand'] && isset($greenfarm_opt['brand_logos'][0]) && $greenfarm_opt['brand_logos'][0]['thumb']!=null) { ?>
				<div class="inner-brands">
					<div class="container">
						<?php if(isset($greenfarm_opt['inner_brand_title']) && $greenfarm_opt['inner_brand_title']!=''){ ?>
							<div class="heading-title style1 ">
								<h3><?php echo esc_html( $greenfarm_opt['inner_brand_title'] ); ?></h3>
							</div>
						<?php } ?>
						<?php echo do_shortcode('[ourbrands]'); ?>
					</div>
				</div>
			<?php }
		}
	?>
	<!-- end brand logo --> 
</div>
<?php get_footer(); ?>